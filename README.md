# ImageProcessing

# Introduction to Image Processing

An image is an array of numbers where information about the brightness of a given channel is encoded as a number that ranges from 0 to 255. There are 
different channels, but the basic ones are:

* Gray: 2D array, 1 channel
* RGB: 3 channels or tensor rank 3
* RGBA: 4 channels or tensor rank 4

The more channels the greater the dimension of the tensor.

## The mathematical way

An image can be seen as a function of two spatial variables: _x_ and _y_ that are the coordinates and the value $z = f(x,y)$ which is the brightness in the given point of space.

The function $z=f(x,y)$ is discrete since each value _x_ and _y_ are the image width and height and the brightness takes values from 0 to 255.

Image processing tasks and the most common techniques:

* Filtering
    * Blur
        * Gaussian 
        * Median
    * Edge detection
        * Canny
        * Sobel 

* Image segmentation

* Object detection

Among many others!

The newer algorithms use deep neural networks (Convolutional Neural Networks) and have a great performance in real world tasks.
